CREATE TABLE `walikota` (
  `kota_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`kota_id`),
  KEY `FKwalikota344707` (`kota_id`),
  CONSTRAINT `FKwalikota344707` FOREIGN KEY (`kota_id`) REFERENCES `kota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;