<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota_fasilitas_model extends CI_Model {

	//function read berfungsi mengambil/read data dari table kota di database
	public function read($kota_id) {

		//sql read
        $this->db->select('kota_fasilitas.*');
        $this->db->select('fasilitas.nama AS nama_fasilitas');
        
        $this->db->from('kota_fasilitas');
        $this->db->join('fasilitas', 'kota_fasilitas.fasilitas_id = fasilitas.id');

        $this->db->where('kota_fasilitas.kota_id', $kota_id);

        $this->db->order_by('fasilitas.nama ASC');
		$query = $this->db->get();

		//$query->result_array = mengirim data ke controller dalam bentuk semua data
        return $query->result_array();
	}

	//function read berfungsi mengambil/read data dari table kota di database
	public function read_single($id) {

		//sql read
		$this->db->select('*');
		$this->db->from('kota_fasilitas');

		//filter data
		$this->db->where('id', $id);

		$query = $this->db->get();

		//query->row_array = mengirim data ke controller dalam bentuk 1 data
        return $query->row_array();
	}

	//function insert berfungsi menyimpan/create data ke table kota di database
	public function insert($input) {
		//$input = data yang dikirim dari controller
		return $this->db->insert('kota_fasilitas', $input);
	}

	//function update berfungsi merubah data ke table kota di database
	public function update($input, $id) {
		//filter data
		$this->db->where('id', $id);

		//$input = data yang dikirim dari controller
		return $this->db->update('kota_fasilitas', $input);
	}

	//function delete berfungsi menghapus data dari table kota di database
	public function delete($id) {
		//filter data
		$this->db->where('id', $id);
		return $this->db->delete('kota_fasilitas');
	}

	//function count berfungsi menghitung total fasilitas per kota
	public function jml_fasil($kota_id) {

		//sql count
		$this->db->select('COUNT(fasilitas_id)');
		$this->db->from('kota_fasilitas');

		//filter data
		$this->db->where('kota_id', $kota_id);

        return $this->db->count_all_results();
	}
}
