<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {

        public function kota_provinsi() {
                $this->db->select('provinsi.nama AS nama_provinsi');
                $this->db->select('kota.nama AS nama_kota');
                $this->db->select('kota.penduduk');
                $this->db->from('kota');
                $this->db->join('provinsi', 'kota.provinsi_id = provinsi.id');
                $this->db->order_by('kota.penduduk DESC');
                $query = $this->db->get();

                return $query->result_array();
        }

        public function jml_fasil_kota() {
                
                $query = $this->db->query('SELECT kota.nama AS nama_kota,
                                                COUNT(kota_fasilitas.fasilitas_id) AS jml_fasil
                                        FROM kota_fasilitas 
                                        JOIN kota ON kota_fasilitas.kota_id = kota.id 
                                        GROUP BY kota.nama');
                
                return $query->result_array();
        }

}
