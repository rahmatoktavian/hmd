<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Walikota_model extends CI_Model {

	//function read berfungsi mengambil/read data dari table walikota di database
	public function read() {

		//sql read
		$this->db->select('walikota.*');
		$this->db->select('kota.nama AS nama_kota');
        $this->db->from('walikota');
        $this->db->join('kota', 'walikota.kota_id = kota.id', 'RIGHT');

        //filter data sesuai id yang dikirim dari controller
		$query = $this->db->get();

		//$query->result_array = mengirim data ke controller dalam bentuk semua data
        return $query->result_array();
	}

	//function read berfungsi mengambil/read data dari table walikota di database
	public function read_single($kota_id) {

		//sql read
		$this->db->select('walikota.*');
		$this->db->select('kota.nama AS nama_kota');
        $this->db->from('walikota');
        $this->db->join('kota', 'walikota.kota_id = kota.id', 'RIGHT');

        //filter data sesuai id yang dikirim dari controller
		$this->db->where('kota.id', $kota_id);
		$query = $this->db->get();

		//$query->row_array = mengirim data ke controller dalam bentuk 1 data
        return $query->row_array();
	}

	//jika kosong : insert, jika sudah ada data : update
	public function replace($input) {
		//$input = data yang dikirim dari controller
		return $this->db->replace('walikota', $input);
	}

	//function insert berfungsi menyimpan/create data ke table walikota di database
	public function insert($input) {
		//$input = data yang dikirim dari controller
		return $this->db->insert('walikota', $input);
	}

	//function update berfungsi merubah data ke table walikota di database
	public function update($input, $kota_id) {
		//$id = id data yang dikirim dari controller (sebagai filter data yang diubah)
		//filter data sesuai id yang dikirim dari controller
		$this->db->where('kota_id', $kota_id);

		//$input = data yang dikirim dari controller
		return $this->db->update('walikota', $input);
	}

	//function delete berfungsi menghapus data dari table walikota di database
	public function delete($kota_id) {
		//$id = id data yang dikirim dari controller (sebagai filter data yang dihapus)
		$this->db->where('kota_id', $kota_id);
		return $this->db->delete('walikota');
	}
}
