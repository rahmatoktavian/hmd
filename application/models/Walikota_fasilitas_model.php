<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Walikota_fasilitas_model extends CI_Model {

	//function read berfungsi mengambil/read data dari table kota di database
	public function read($kota_id) {

		//sql read
        $this->db->select('walikota_fasilitas.*');
        $this->db->select('fasilitas.nama AS nama_fasilitas');
        
        $this->db->from('walikota_fasilitas');
        $this->db->join('fasilitas', 'walikota_fasilitas.fasilitas_id = fasilitas.id');

        $this->db->where('walikota_fasilitas.kota_id', $kota_id);

        $this->db->order_by('fasilitas.nama ASC');
		$query = $this->db->get();

		//$query->result_array = mengirim data ke controller dalam bentuk semua data
        return $query->result_array();
	}

	//function read berfungsi mengambil/read data dari table kota di database
	public function read_single($kota_id, $fasilitas_id) {

		//sql read
		$this->db->select('*');
		$this->db->from('walikota_fasilitas');

		//filter data
		$this->db->where('kota_id', $kota_id);
		$this->db->where('fasilitas_id', $fasilitas_id);

		$query = $this->db->get();

		//query->row_array = mengirim data ke controller dalam bentuk 1 data
        return $query->row_array();
	}

	public function replace($input) {
		//$input = data yang dikirim dari controller
		return $this->db->replace('walikota_fasilitas', $input);
	}

	//function insert berfungsi menyimpan/create data ke table kota di database
	public function insert($input) {
		//$input = data yang dikirim dari controller
		return $this->db->insert('walikota_fasilitas', $input);
	}

	//function update berfungsi merubah data ke table kota di database
	public function update($input, $kota_id, $fasilitas_id) {
		//filter data
		$this->db->where('kota_id', $kota_id);
		$this->db->where('fasilitas_id', $fasilitas_id);

		//$input = data yang dikirim dari controller
		return $this->db->update('walikota_fasilitas', $input);
	}

	//function delete berfungsi menghapus data dari table kota di database
	public function delete($kota_id, $fasilitas_id) {
		//filter data
		$this->db->where('kota_id', $kota_id);
		$this->db->where('fasilitas_id', $fasilitas_id);

		return $this->db->delete('walikota_fasilitas');
	}
}
