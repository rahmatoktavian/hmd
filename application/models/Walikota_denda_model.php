<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Walikota_denda_model extends CI_Model {

	//function read berfungsi mengambil/read data dari table walikota_denda di database
	public function read($kota_id) {

		//sql read
		$this->db->select('walikota_denda.*');
        $this->db->select('denda.jenis AS jenis_denda');
		$this->db->from('walikota_denda');
		$this->db->join('denda', 'walikota_denda.denda_id = denda.id');

		$this->db->where('kota_id', $kota_id);

		$query = $this->db->get();

		//$query->result_array = mengirim data ke controller dalam bentuk semua data
        return $query->result_array();
	}

	//function read berfungsi mengambil/read data dari table walikota_denda di database
	public function read_single($kota_id, $denda_id) {

		//sql read
		$this->db->select('*');
		$this->db->from('walikota_denda');

		//$id = id data yang dikirim dari controller (sebagai filter data yang dipilih)
		//filter data sesuai id yang dikirim dari controller
		$this->db->where('kota_id', $kota_id);
		$this->db->where('denda_id', $denda_id);

		$query = $this->db->get();

		//query->row_array = mengirim data ke controller dalam bentuk 1 data
        return $query->row_array();
	}

	//function insert berfungsi menyimpan/create data ke table walikota_denda di database
	public function insert($input) {
		//$input = data yang dikirim dari controller
		return $this->db->insert('walikota_denda', $input);
	}

	//function update berfungsi merubah data ke table walikota_denda di database
	public function update($input, $kota_id, $denda_id) {
		//$id = id data yang dikirim dari controller (sebagai filter data yang diubah)
		//filter data sesuai id yang dikirim dari controller
		$this->db->where('kota_id', $kota_id);
		$this->db->where('denda_id', $denda_id);

		//$input = data yang dikirim dari controller
		return $this->db->update('walikota_denda', $input);

		/*
		UPDATE walikota_denda
		SET nama = 'Jakarta'
		WHERE id = 1
		*/
	}

	//function delete berfungsi menghapus data dari table walikota_denda di database
	public function delete($kota_id) {
		//$id = id data yang dikirim dari controller (sebagai filter data yang dihapus)
		$this->db->where('kota_id', $kota_id);

		return $this->db->delete('walikota_denda');

		//DELETE FROM walikota_denda WHERE id = 1
	}
}
