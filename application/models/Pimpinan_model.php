<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pimpinan_model extends CI_Model {

	//function read berfungsi mengambil/read data dari table pimpinan di database
	public function read_single($id_kota) {

		//sql read
		$this->db->select('kota.id AS id_kota');
		$this->db->select('kota.nama AS nama_kota');
        $this->db->select('pimpinan.nama AS nama_pimpinan');
        $this->db->from('pimpinan');
        $this->db->join('kota', 'pimpinan.id_kota = kota.id', 'RIGHT');

        //filter data sesuai id yang dikirim dari controller
		$this->db->where('kota.id', $id_kota);

		$query = $this->db->get();

		//$query->result_array = mengirim data ke controller dalam bentuk semua data
        return $query->row_array();
	}

	public function replace($input) {
		//$input = data yang dikirim dari controller
		return $this->db->replace('pimpinan', $input);
	}

	//function insert berfungsi menyimpan/create data ke table pimpinan di database
	public function insert($input) {
		//$input = data yang dikirim dari controller
		return $this->db->insert('pimpinan', $input);
	}

	//function update berfungsi merubah data ke table pimpinan di database
	public function update($input, $id_kota) {
		//$id = id data yang dikirim dari controller (sebagai filter data yang diubah)
		//filter data sesuai id yang dikirim dari controller
		$this->db->where('id_kota', $id_kota);

		//$input = data yang dikirim dari controller
		return $this->db->update('pimpinan', $input);
	}

	//function delete berfungsi menghapus data dari table pimpinan di database
	public function delete($id_kota) {
		//$id = id data yang dikirim dari controller (sebagai filter data yang dihapus)
		$this->db->where('id_kota', $id_kota);
		return $this->db->delete('pimpinan');
	}
}
