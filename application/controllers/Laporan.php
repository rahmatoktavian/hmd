<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct() {
        parent::__construct();

        //memanggil model
        $this->load->model('laporan_model');
    }

	public function index() {
		//mengarahkan ke function read
		$this->kota_provinsi();
	}

	public function kota_provinsi() {
		//memanggil fungsi model laporan
		$data_laporan = $this->laporan_model->kota_provinsi();
		
		//mengirim data ke view
		$output = array(
						'theme_page' => 'laporan_kota_provinsi',
						'judul' => 'Urutan Penduduk Kota Terdapat per Provinsi',
						'data_laporan' => $data_laporan
					);

		//memanggil file view
		$this->load->view('theme/index', $output);
	}

	public function kota_provinsi_export() {
		$tipe_file = $this->uri->segment(3);

		//memanggil fungsi model laporan
		$data_laporan = $this->laporan_model->kota_provinsi();
		
		//mengirim data ke view
		$output = array(
						'data_laporan' => $data_laporan,
						'tipe_file' => $tipe_file
					);

		//excel
		if($tipe_file == 'xls') {
			$this->load->view('laporan_kota_provinsi_export', $output);

		//pdf
		} else {
	    	$this->pdf->view('laporan_kota_provinsi_export', $output);
		}
	}

	public function jml_fasil_kota() {
		//memanggil fungsi model laporan
		$data_laporan = $this->laporan_model->jml_fasil_kota();

		//mengirim data ke view
		$output = array(
						'theme_page' => 'laporan_jml_fasil_kota',
						'judul' => 'Laporan Jumlah Fasilitas Kota',
						'data_laporan' => $data_laporan
					);

		//memanggil file view
		$this->load->view('theme/index', $output);
	}

	public function jml_fasil_kota_export($tipe_file) {
		//memanggil fungsi model laporan
		$data_laporan = $this->laporan_model->jml_fasil_kota();
		
		//mengirim data ke view
		$output = array(
						'data_laporan' => $data_laporan,
						'tipe_file' => $tipe_file
					);

		//excel
		if($tipe_file == 'xls') {
			$this->load->view('laporan_jml_fasil_kota_export', $output);

		//pdf
		} else {
			$this->load->library('pdf');
	    	$this->pdf->view('laporan_jml_fasil_kota_export', $output);
		}
	}
}
