<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota_fasilitas extends CI_Controller {

	public function __construct() {
        parent::__construct();

        //memanggil model
        $this->load->model(array('kota_model','fasilitas_model','kota_fasilitas_model'));
    }

	public function index() {
		//mengarahkan ke function read
		$this->read();
	}

	public function read() {
		//menangkap id kota dari view : kota read
		$kota_id_url = $this->uri->segment(3);

		//untuk menampilkan data kota induk
		$data_kota = $this->kota_model->read_single($kota_id_url);

		//untuk menampilkan daftar fasilitas kota (table)
		$data_kota_fasilitas = $this->kota_fasilitas_model->read($kota_id_url);

		//mengirim data ke view
		$output = array(
						'judul' => 'Daftar Kota Fasilitas',

						//id kota dibawa agar filter fasilitas per kota
						'kota_id_url' => $kota_id_url,

						'data_kota' => $data_kota,
						'data_kota_fasilitas' => $data_kota_fasilitas
					);

		//memanggil file view
		$this->load->view('kota_fasilitas_read', $output);
	}

	public function insert() {
		
		//menangkap id kota url
		$kota_id_url = $this->uri->segment(3);

		//untuk menampilkan data fasilitas
		$data_fasilitas = $this->fasilitas_model->read();

		//mengirim data ke view
		$output = array(
						'judul' => 'Tambah Kota Fasilitas',

						//id kota dibawa agar filter fasilitas per kota
						'kota_id_url' => $kota_id_url,

						//data dropdown
						'data_fasilitas' => $data_fasilitas
					);

		//memanggil file view
		$this->load->view('kota_fasilitas_insert', $output);
	}

	public function insert_submit() {
		//menangkap id kota url
		$kota_id_url = $this->uri->segment(3);

		//menangkap data input dari view
		$fasilitas_id_post = $this->input->post('fasilitas_id');
		$nama_post = $this->input->post('nama');

		//start proses transaction
		$this->db->trans_begin();

		//proses insert kota_fasilitas
		$input = array(
						//id kota diambil dari URL
						'kota_id' => $kota_id_url,

						'fasilitas_id' => $fasilitas_id_post,
						'nama' => $nama_post,
					);
		$this->kota_fasilitas_model->insert($input);

		//proses menghitung total fasilitas
		$jml_fasil = $this->kota_fasilitas_model->jml_fasil($kota_id_url);

		//update jml fasil table kota
		$input_jml_fasil = array(
						'jml_fasil' => $jml_fasil
					);
		$this->kota_model->update($input_jml_fasil, $kota_id_url);

		//jika semua proses benar (jalankan semua proses)
		if ($this->db->trans_status() === TRUE) {
		    $this->db->trans_commit();

		//jika ada 1 proses yang salah (batalkan semua proses)
		} else {
		    $this->db->trans_rollback();
		}

		//mengembalikan halaman ke function read dgn id kota
		redirect('kota_fasilitas/read/'.$kota_id_url);
	}

	public function update() {
		//menangkap url
		$kota_id_url = $this->uri->segment(3);
		$id = $this->uri->segment(4);

		//data fasilitas yg dipilih
		$data_kota_fasilitas_single = $this->kota_fasilitas_model->read_single($id);

		//untuk menampilkan data fasilitas
		$data_fasilitas = $this->fasilitas_model->read();

		//mengirim data ke view
		$output = array(
						'judul' => 'Ubah Kota Fasilitas',

						//id kota diambil dari URL
						'kota_id_url' => $kota_id_url,

						//mengirim data kota_fasilitas yang dipilih ke view
						'data_kota_fasilitas_single' => $data_kota_fasilitas_single,

						//data dropdown
						'data_fasilitas' => $data_fasilitas
					);

		//memanggil file view
		$this->load->view('kota_fasilitas_update', $output);
	}

	public function update_submit() {
		//menangkap id url
		$kota_id_url = $this->uri->segment(3);
		$id = $this->uri->segment(4);

		//menangkap data input dari view
		$fasilitas_id_post = $this->input->post('fasilitas_id');
		$nama_post = $this->input->post('nama');

		//mengirim data ke model
		$input = array(
						//format : nama field/kolom table => data input dari view
						'fasilitas_id' => $fasilitas_id_post,
						'nama' => $nama_post,
					);

		//memanggil function update pada kota_fasilitas model
		//function update berfungsi merubah data ke table kota_fasilitas di database
		$this->kota_fasilitas_model->update($input, $id);

		//mengembalikan halaman ke function read dengan id kota
		redirect('kota_fasilitas/read/'.$kota_id_url);
	}

	public function delete() {
		//menangkap id url
		$kota_id_url = $this->uri->segment(3);
		$id = $this->uri->segment(4);

		//proses delete kota_fasilitas
		$this->kota_fasilitas_model->delete($id);

		//proses menghitung total fasilitas
		$jml_fasil = $this->kota_fasilitas_model->jml_fasil($kota_id_url);

		//update jml fasil table kota
		$input_jml_fasil = array(
						'jml_fasil' => $jml_fasil
					);
		$this->kota_model->update($input_jml_fasil, $kota_id_url);

		//jika semua proses benar (jalankan semua proses)
		if ($this->db->trans_status() === TRUE) {
		    $this->db->trans_commit();

		//jika ada 1 proses yang salah (batalkan semua proses)
		} else {
		    $this->db->trans_rollback();
		}
		

		//mengembalikan halaman ke function read dengan id kota
		redirect('kota_fasilitas/read/'.$kota_id_url);
	}
}
