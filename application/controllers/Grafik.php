<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik extends CI_Controller {

	public function __construct() {
        parent::__construct();

        //memanggil model
        $this->load->model('laporan_model');
    }

	public function index() {
		//mengarahkan ke function read
		$this->kota_provinsi();
	}

	public function kota_provinsi() {
		//memanggil fungsi model laporan
		$data_grafik = $this->laporan_model->kota_provinsi();
		
		//mengirim data ke view
		$output = array(
						'theme_page' => 'grafik_kota_provinsi',
						'judul' => 'Perbandingan Penduduk Antar Kota',
						'data_grafik' => $data_grafik
					);

		//memanggil file view
		$this->load->view('theme/index', $output);
	}

	public function jml_fasil_kota() {
		//memanggil fungsi model laporan
		$data_grafik = $this->laporan_model->jml_fasil_kota();

		//mengirim data ke view
		$output = array(
						'theme_page' => 'grafik_jml_fasil_kota',
						'judul' => 'Perbandingan Fasilitas Antar Kota',
						'data_grafik' => $data_grafik
					);

		//memanggil file view
		$this->load->view('theme/index', $output);
	}

}
