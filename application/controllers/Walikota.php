<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Walikota extends CI_Controller {

	public function __construct() {
        parent::__construct();

        //memanggil model
        $this->load->model(array('walikota_model','walikota_fasilitas_model','denda_model','walikota_denda_model'));
    }

	public function index() {
		//mengarahkan ke function read
		$this->update();
	}

	public function update() {
		//menangkap id data yg dipilih dari view (parameter get)
		$kota_id_url = $this->uri->segment(3);

		//data walikota berdasarkan id kota
		$data_walikota_single = $this->walikota_model->read_single($kota_id_url);

		//daftar fasilitas berdasarkan id kota
		$data_walikota_fasilitas = $this->walikota_fasilitas_model->read($kota_id_url);

		//data walikota denda berdasarkan id kota
		$data_walikota_denda = $this->walikota_denda_model->read($kota_id_url);

		//status fasilitas
		$data_status_fasilitas = array(
									'0' => 'Baik',
									'1' => 'Perlu Diperbaiki',
									'2' => 'Rusak',
								);

		//mengirim data ke view
		$output = array(
						'judul' => 'Walikota',

						//mengirim id kota ke view
						'kota_id_url' => $kota_id_url,

						'data_walikota_single' => $data_walikota_single,
						'data_walikota_fasilitas' => $data_walikota_fasilitas,
						'data_status_fasilitas' => $data_status_fasilitas,
						'data_walikota_denda' => $data_walikota_denda

					);

		//memanggil file view
		$this->load->view('walikota_update', $output);
	}

	public function update_submit() {
		//menangkap id data yg dipilih dari view
		$kota_id_url = $this->uri->segment(3);

		//menangkap data input dari view
		$nama = $this->input->post('nama');

		//mengirim data ke model
		$input = array(
					'kota_id' => $kota_id_url,
					'nama' => $nama,
				);

		//replace : jika belum ada data -> insert, jika sudah -> update
		//hanya bisa digunakan tipe 1 - 1
		$this->walikota_model->replace($input);

		//mengembalikan halaman ke function read
		redirect('walikota/update/'.$kota_id_url);
	}

	public function update_fasilitas_submit() {
		//menangkap id data yg dipilih dari view
		$kota_id_url = $this->uri->segment(3);
		
		//menangkap checkbox fasilitas dari view
		$status_array = $this->input->post('status');

		//delete data walikota denda
		$this->walikota_denda_model->delete($kota_id_url);

		//looping checkbox fasilitas
		foreach($status_array as $fasilitas_id=>$status) {

			//mengirim data ke model
			$input_walikota_fasilitas = array(
										'status' => $status,
									);
			$this->walikota_fasilitas_model->update($input_walikota_fasilitas, $kota_id_url, $fasilitas_id);

			//ambil data denda sesuai status
			$data_denda = $this->denda_model->read_single($status);
			
			//jika ada denda, maka proses denda
			if(!empty($data_denda)) {

				//nominal denda sesuai status
				$nominal_denda = $data_denda['biaya'];

				//check data walikota denda sudah ada atau belum
				$data_walikota_denda = $this->walikota_denda_model->read_single($kota_id_url, $status);

				//jika ada, maka update (data yg ada + data baru)
				if(!empty($data_walikota_denda)) {
					$input_walikota_denda = array(
											'qty' => $data_walikota_denda['qty'] + 1,
											'nominal' =>  $data_walikota_denda['nominal'] + $nominal_denda,
										);

					$this->walikota_denda_model->update($input_walikota_denda, $kota_id_url, $status);

				//jika belum ada, maka insert
				} else {
					$input_walikota_denda = array(
											'kota_id' => $kota_id_url,
											'denda_id' => $status,
											'qty' => 1,
											'nominal' => $nominal_denda,
										);

					$this->walikota_denda_model->insert($input_walikota_denda);
				}
			}
		}

		//mengembalikan halaman ke function read
		redirect('walikota/update/'.$kota_id_url);
	}
}
