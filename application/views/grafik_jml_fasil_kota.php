<div id="grafik_jml_fasil_kota"></div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script type="text/javascript">
	// Build the chart
	Highcharts.chart('grafik_jml_fasil_kota', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<?php echo $judul;?>'
	    },
	    xAxis: {
	        categories: ['Fasilitas'],

	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Jumlah Fasilitas'
	        }
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },

	    series: [
	    			<?php foreach($data_grafik as $grafik):?>
	        		{	
	        			name: '<?php echo $grafik['nama_kota'];?>', 
	        			data: [<?php echo $grafik['jml_fasil'];?>]
	        		},
			        <?php endforeach?>
				]
	    /*
	    series: [
		    		{
				        name: 'Tokyo',
				        data: [49.9]
				    }, 
				    {
				        name: 'New York',
				        data: [83.6]
				    }, 
				    {
				        name: 'London',
				        data: [48.9]
				    }, 
				    {
				        name: 'Berlin',
				        data: [42.4]
					}
				]
		*/

	});
</script>