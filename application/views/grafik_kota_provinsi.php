<div id="grafik_kota_provinsi"></div>

<script src="<?php echo base_url('assets/vendor/highcharts/highcharts.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/highcharts/modules/exporting.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/highcharts/modules/export-data.js');?>"></script>
<script type="text/javascript">
	// Build the chart
	Highcharts.chart('grafik_kota_provinsi', {
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        type: 'pie'
	    },
	    title: {
	        text: '<?php echo $judul;?>'
	    },
	    tooltip: {
	        pointFormat: '{series.name}: <b>{point.y}</b> (<b>{point.percentage:.1f}%</b>)'
	    },
	    accessibility: {
	        point: {
	            valueSuffix: '%'
	        }
	    },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: false
	            },
	            showInLegend: true
	        }
	    },
	    series: [{
	        name: 'Penduduk',
	        colorByPoint: true,

	        //format data penduduk kota
	        data: [
	        		<?php foreach($data_grafik as $grafik):?>
	        		{	
	        			name: '<?php echo $grafik['nama_kota'];?>', 
	        			y: <?php echo $grafik['penduduk'];?>
	        		},
			        <?php endforeach?>
			   	]

	        //format data original
	        /*
	        data: [
	        		{
			            name: 'Chrome', 
			            y: 61.41
			        }, 
			        {
			            name: 'Internet Explorer',
			            y: 11.84
			        }, 
			        {
			            name: 'Firefox, 
			            y: 10.85
			        },
			   	]
			*/
	    }]
	});
</script>