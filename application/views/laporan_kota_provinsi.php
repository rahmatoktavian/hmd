<table class="table table-striped">
	<thead class="thead-dark">
		<tr>
			<th>Nama Provinsi</th>
			<th>Nama Kota</th>
			<th>Penduduk</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($data_laporan as $laporan):?>
		<tr>
			<td><?php echo $laporan['nama_provinsi'];?></td>
			<td><?php echo $laporan['nama_kota'];?></td>
			<td><?php echo number_format($laporan['penduduk']);?></td>
		</tr>
		<?php endforeach?>		
	</tbody>
</table>

<a href="<?php echo site_url('laporan/kota_provinsi_export/xls');?>" class="btn btn-success">
<i class="fa fa-download"></i> Excel
</a>

<a href="<?php echo site_url('laporan/kota_provinsi_export/pdf');?>" class="btn btn-danger">
<i class="fa fa-download"></i> PDF
</a>