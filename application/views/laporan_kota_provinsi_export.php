<?php
if($tipe_file == 'xls') {
	header( "Content-Type: application/vnd.ms-excel" );
	header( "Content-disposition: attachment; filename=".$_ci_view.".xls" );
}
?>

<table border="1">
	<thead>
		<tr>
			<th>Nama Provinsi</th>
			<th>Nama Kota</th>
			<th>Penduduk</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($data_laporan as $laporan):?>
		<tr>
			<td><?php echo $laporan['nama_provinsi'];?></td>
			<td><?php echo $laporan['nama_kota'];?></td>
			<td><?php echo number_format($laporan['penduduk']);?></td>
		</tr>
		<?php endforeach?>		
	</tbody>
</table>
