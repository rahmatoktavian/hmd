<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $judul?></title>
</head>
<body>

<h1><?php echo $judul?></h1>

<!--$data_kota_single['id'] : perlu diletakan di url agar bisa diterima/tangkap pada controller (sbg penanda id yang akan diupdate) -->
<form method="post" action="<?php echo site_url('kota_fasilitas/update_submit/'.$kota_id_url.'/'.$data_kota_fasilitas_single['id']);?>">
	<table>
		<tr>
			<td>Fasilitas</td>
			<td>
				<select name="fasilitas_id">
				<?php foreach($data_fasilitas as $fasilitas):?>

					<!--jika id fasilitas yang sedang loop = id fasilitas read single : dropdown dipilih default-->
					<?php if($fasilitas['id'] == $data_kota_fasilitas_single['fasilitas_id']):?>
						<option value="<?php echo $fasilitas['id'];?>" selected><?php echo $fasilitas['nama'];?></option>

					<?php else:?>
						<option value="<?php echo $fasilitas['id'];?>"><?php echo $fasilitas['nama'];?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Nama</td>
			<!--$data_kota_single['nama'] : menampilkan data kota yang dipilih dari database -->
			<td><input type="text" name="nama" value="<?php echo $data_kota_fasilitas_single['nama'];?>" required=""></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="submit" value="Simpan"></td>
		</tr>
	</table>
</form>

</body>
</html>