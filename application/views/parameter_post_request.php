<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $judul;?></title>
</head>
<body>

<h3><?php echo $judul;?></h3>

<form method="post" action="<?php echo site_url('parameter/post_response/');?>">
	<table border="1">
		<tr>
			<td>Nama</td>
			<td><input type="text" name="nama" value=""></td>
		</tr>
		<tr>
			<td>Umur</td>
			<td><input type="number" name="umur" value=""></td>
		</tr>
		<tr>
			<td>Tanggal Lahir</td>
			<td><input type="date" name="tanggal_lahir" value=""></td>
		</tr>
		<tr>
			<td>Gender</td>
			<td>
				<input type="radio" name="gender" value="pria"> Pria
				<input type="radio" name="gender" value="wanita"> Wanita
			</td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>
				<select name="provinsi">
					<option value="Jakarta">Jakarta</option>
					<option value="Jawa Barat">Jawa Barat</option>
					<option value="Jawa Tengah">Jawa Tengah</option>
					<option value="Jawa Timur">Jawa Timur</option>
					<option value="Bali">Bali</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Hobi</td>
			<td>
				<input type="checkbox" name="hobi" value="Baca"> Baca
				<input type="checkbox" name="hobi" value="Olahraga"> Olahraga
				<input type="checkbox" name="hobi" value="Musik"> Musik
			</td>
		</tr>
		<tr>
			<td></td>
			<td><button type="submit">Tampilkan</button></td>
		</tr>
	</table>
</form>

</body>
</html>