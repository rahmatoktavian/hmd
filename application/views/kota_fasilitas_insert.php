<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $judul?></title>

</head>
<body>

<h1><?php echo $judul?></h1>


<form method="post" action="<?php echo site_url('kota_fasilitas/insert_submit/'.$kota_id_url);?>">
	<table>
		<tr>
			<td>Fasilitas</td>
			<td>
				<select name="fasilitas_id">
				<?php foreach($data_fasilitas as $fasilitas):?>
					<option value="<?php echo $fasilitas['id'];?>">
						<?php echo $fasilitas['nama'];?>	
					</option>
				<?php endforeach;?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td><input type="text" name="nama" value="" required=""></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="submit" value="Simpan" class="btn btn-primary"></td>
		</tr>
	</table>
</form>

</body>
</html>