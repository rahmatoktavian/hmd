<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $judul?></title>
</head>
<body>

<h1><?php echo $judul?></h1>

<a href="<?php echo site_url('kota/read/');?>">Pilih Kota Lain</a>
<br /><br />

<table border="1" width="100%">
	<tr>
		<td width="10%">Nama Kota</td>
		<td><?php echo $data_kota['nama'];?></td>
	</tr>
	<tr>
		<td>Jumlah Fasilitas</td>
		<td><?php echo $data_kota['jml_fasil'];?></td>
	</tr>
</table>
<br />

<a href="<?php echo site_url('kota_fasilitas/insert/'.$kota_id_url);?>">Tambah</a>
<br /><br />

<table border="1" width="100%">
	<thead>
		<tr>
			<th width="10%">Fasiltas</th>
			<th>Nama</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($data_kota_fasilitas as $kota_fasilitas):?>
		<tr>
			<td><?php echo $kota_fasilitas['nama_fasilitas'];?></td>
			<td><?php echo $kota_fasilitas['nama'];?></td>
			<td>
				<a href="<?php echo site_url('kota_fasilitas/update/'.$kota_id_url.'/'.$kota_fasilitas['id']);?>">
				Ubah
				</a>
				|
				<a href="<?php echo site_url('kota_fasilitas/delete/'.$kota_id_url.'/'.$kota_fasilitas['id']);?>" onClick="return confirm('Anda yakin menghapus?')">
				Hapus
				</a>
			</td>
		</tr>
		<?php endforeach?>		
	</tbody>
</table>

</body>
</html>