<a href="<?php echo site_url('provinsi/insert');?>" class="btn btn-primary">Tambah</a>
<br /><br />

<table border="1">
	<thead class="thead-dark">
		<tr>
			<th>Nama</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($data_provinsi as $provinsi):?>
		<tr>
			<td><?php echo $provinsi['nama'];?></td>
			<td>
				<a href="<?php echo site_url('provinsi/update/'.$provinsi['id']);?>">
				Ubah
				</a>
				|
				<a href="<?php echo site_url('provinsi/delete/'.$provinsi['id']);?>" onClick="return confirm('Anda yakin?')">
				Hapus
				</a>
			</td>
		</tr>
		<?php endforeach?>		
	</tbody>
</table>