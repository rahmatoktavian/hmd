<?php
if($tipe_file == 'xls') {
	header( "Content-Type: application/vnd.ms-excel" );
	header( "Content-disposition: attachment; filename=".$_ci_view.".xls" );
}
?>

<table border="1">
	<thead>
		<tr>
			<th>Nama Kota</th>
			<th>Jumlah Fasilitas</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($data_laporan as $laporan):?>
		<tr>
			<td><?php echo $laporan['nama_kota'];?></td>
			<td><?php echo number_format($laporan['jml_fasil']);?></td>
		</tr>
		<?php endforeach?>		
	</tbody>
</table>
