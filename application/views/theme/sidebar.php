<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Input -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu_input" aria-expanded="true" aria-controls="menu_input">
      <i class="fas fa-fw fa-folder"></i>
      <span>Input Data</span>
    </a>
    <div id="menu_input" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="<?php echo site_url('provinsi/read');?>">Provinsi</a>
        <a class="collapse-item" href="<?php echo site_url('kota/read');?>">Kota</a>
      </div>
    </div>
  </li>

  <!-- Laporan -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu_laporan" aria-expanded="true" aria-controls="menu_laporan">
      <i class="fas fa-fw fa-folder"></i>
      <span>Laporan</span>
    </a>
    <div id="menu_laporan" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="<?php echo site_url('laporan/kota_provinsi');?>">Daftar Kota & Provinsi</a>
        <a class="collapse-item" href="<?php echo site_url('laporan/jml_fasil_kota');?>">Jumlah Fasilitas Kota</a>
      </div>
    </div>
  </li>

  <!-- Grafik -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu_grafik" aria-expanded="true" aria-controls="menu_grafik">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Grafik</span>
    </a>
    <div id="menu_grafik" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="<?php echo site_url('grafik/kota_provinsi');?>">Penduduk Kota</a>
        <a class="collapse-item" href="<?php echo site_url('grafik/jml_fasil_kota');?>">Jumlah Fasilitas Kota</a>
      </div>
    </div>
  </li>

  <hr class="sidebar-divider d-none d-md-block">

  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>