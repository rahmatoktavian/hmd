--RUN SQL : ALTER TABLE KOTA
ALTER TABLE `kota` ADD `jml_fasil` double NOT NULL DEFAULT '0';

--RUN TRIGGER (NOT SQL) : AFTER INSERT (kota)
BEGIN
    SELECT COUNT(fasilitas_id)
            INTO @jml_fasil
    FROM kota_fasilitas
    WHERE kota_id = NEW.kota_id;

    UPDATE kota
    SET jml_fasil = @jml_fasil
    WHERE id = NEW.kota_id;
END

--RUN TRIGGER (NOT SQL) : AFTER DELETE (kota)
BEGIN
    SELECT COUNT(fasilitas_id)
            INTO @jml_fasil
    FROM kota_fasilitas
    WHERE kota_id = OLD.kota_id;

    UPDATE kota
    SET jml_fasil = @jml_fasil
    WHERE id = OLD.kota_id;
END