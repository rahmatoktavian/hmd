CREATE TABLE `kota_fasilitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kota_id` int(11) NOT NULL,
  `fasilitas_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkota_fasil794204` (`kota_id`),
  KEY `FKkota_fasil983781` (`fasilitas_id`),
  CONSTRAINT `FKkota_fasil794204` FOREIGN KEY (`kota_id`) REFERENCES `kota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKkota_fasil983781` FOREIGN KEY (`fasilitas_id`) REFERENCES `fasilitas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TRIGGER `kota_fasilitas_ai` AFTER INSERT ON `kota_fasilitas` FOR EACH ROW
INSERT INTO walikota_fasilitas
(kota_id, fasilitas_id)
VALUES
(NEW.kota_id, NEW.fasilitas_id);;

CREATE TRIGGER `kota_fasilitas_ad` AFTER DELETE ON `kota_fasilitas` FOR EACH ROW
DELETE FROM walikota_fasilitas
WHERE kota_id = OLD.kota_id
AND fasilitas_id = OLD.fasilitas_id;;