CREATE TABLE `kota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provinsi_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `penduduk` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkota472521` (`provinsi_id`),
  CONSTRAINT `FKkota472521` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;