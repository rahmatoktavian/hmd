CREATE TABLE `denda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(255) NOT NULL,
  `biaya` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `denda` (`id`, `jenis`, `biaya`) VALUES
(1,	'Perlu Diperbaiki',	1500000),
(2,	'Rusak',	2000000);