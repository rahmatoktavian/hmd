CREATE TABLE `walikota_fasilitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kota_id` int(11) NOT NULL,
  `fasilitas_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FKwalikota_f409028` (`kota_id`),
  KEY `FKwalikota_f421532` (`fasilitas_id`),
  CONSTRAINT `FKwalikota_f409028` FOREIGN KEY (`kota_id`) REFERENCES `walikota` (`kota_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKwalikota_f421532` FOREIGN KEY (`fasilitas_id`) REFERENCES `fasilitas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;